package android.rnita.me.asynctaskloaderexample;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.graphics.Bitmap;

public class DownloadImageAsyncTaskLoaderHelper extends AsyncTaskLoader<Bitmap> {

    private String imageUrl = "";
    private Context context = null;

    public DownloadImageAsyncTaskLoaderHelper(Context context, String url) {
        super(context);

        this.imageUrl = url;
        this.context = context;
    }

    @Override
    public Bitmap loadInBackground() {
        try {
            return HttpUtil.getBitmapHttpService(context, imageUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
